package com.pharmeasy.carbon.client.controller;

import com.pharmeasy.carbon.client.CarbonClient;
import com.pharmeasy.carbon.client.model.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@RestController
public class TestController {
    private static final Logger logger = LoggerFactory.getLogger(TestController.class);

    @Autowired
    CarbonClient carbonClient;



    @GetMapping("/ruleSetVersions")
    Mono<Object> getRuleSetVersions(
            @RequestParam(name = "ruleSetId") String ruleSetId,
            @RequestParam(name = "ruleSetVersionId") String ruleSetVersionId) {
        return Mono.just(String.class).zipWith(carbonClient.getRuleSetVersions(ruleSetId, ruleSetVersionId)).flatMap(
                ruleSet -> Mono.just(ruleSet.getT2())
        );
    }

    @PostMapping("/evaluate")
    Object getResult(@RequestBody Request request) {
        return carbonClient.evaluate(
                request.getRuleSetId(),
                request.getRuleSetVersionId(),
                request.getFacts(),
                request.isShowResults()
        );
    }

}
