package com.pharmeasy.carbon.client.model;

import lombok.Data;

import java.util.Map;

@Data
public class Request {
    private String ruleSetId;
    private String ruleSetVersionId;
    private Map<String, Object> facts;
    private boolean showResults;
}
