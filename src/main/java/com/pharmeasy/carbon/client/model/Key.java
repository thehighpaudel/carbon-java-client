package com.pharmeasy.carbon.client.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@AllArgsConstructor
@EqualsAndHashCode
public class Key {
    String ruleSetId;
    String ruleSetVersionId;
}
