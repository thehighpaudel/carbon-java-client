package com.pharmeasy.carbon.client.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Response {
    String code;
    String message;
}
