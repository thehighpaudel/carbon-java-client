package com.pharmeasy.carbon.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.benmanes.caffeine.cache.AsyncCacheLoader;
import com.github.benmanes.caffeine.cache.AsyncLoadingCache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.pharmeasy.carbon.client.config.CarbonClientProperties;
import com.pharmeasy.carbon.client.model.Key;
import com.pharmeasy.carbon.client.model.Response;
import com.pharmeasy.carbon.client.pollers.RuleSetVersionPoller;
import com.pharmeasy.carbon.client.pollers.ScriptPoller;
import com.pharmeasy.carbon.client.util.ScriptExecutorService;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.circuitbreaker.CircuitBreakerConfig;
import io.github.resilience4j.circuitbreaker.CircuitBreakerRegistry;
import io.github.resilience4j.reactor.circuitbreaker.operator.CircuitBreakerOperator;
import io.github.resilience4j.timelimiter.TimeLimiterConfig;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@Component
@Slf4j
@RequiredArgsConstructor
public class CarbonClient {

    private final static String SCRIPT_KEY = "JS_CODE";

    private final ScriptPoller scriptPoller;
    private final RuleSetVersionPoller ruleSetVersionPoller;
    private final CarbonClientProperties carbonClientProperties;
    private final WebClient carbonWebClient;

    ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();

    @Autowired(required = false)
    CircuitBreakerRegistry circuitBreakerRegistry;

    CircuitBreaker circuitBreaker;

    private AsyncLoadingCache<String, Invocable> jsScriptCache;
    private AsyncLoadingCache<Key, String> ruleSetVersionsCache;
    private AsyncLoadingCache<String, String> currentlyLiveSetVersionCache;


    @PostConstruct
    public void initialize() {
        if (circuitBreakerRegistry == null) {
            CircuitBreakerConfig circuitBreakerConfig = CircuitBreakerConfig.custom()
                    .failureRateThreshold(10)
                    .slowCallRateThreshold(50)
                    .waitDurationInOpenState(Duration.ofSeconds(60))
                    .slowCallDurationThreshold(Duration.ofSeconds(2))
                    .permittedNumberOfCallsInHalfOpenState(3)
                    .minimumNumberOfCalls(5)
                    .slidingWindowType(CircuitBreakerConfig.SlidingWindowType.COUNT_BASED)
                    .slidingWindowSize(100)
                    .build();
            circuitBreaker = CircuitBreaker.of("carbon-client", circuitBreakerConfig);
            TimeLimiterConfig timeLimiterConfig = TimeLimiterConfig.custom().timeoutDuration(Duration.ofSeconds(1)).build();
        } else {
            circuitBreaker = circuitBreakerRegistry.circuitBreaker("carbon-client");
        }

        fetchJsScriptAndCache();
        fetchRuleSetVersionsFromRuleEngine();
        fetchLiveRuleSetVersionsFromRuleEngine();

        scriptPoller.startScriptPoller(scheduledExecutorService, jsScriptCache);
        ruleSetVersionPoller.startRuleSetVersionPoller(scheduledExecutorService, currentlyLiveSetVersionCache);
    }

    private void fetchLiveRuleSetVersionsFromRuleEngine() {
        currentlyLiveSetVersionCache = Caffeine
                .newBuilder()
                .maximumSize(10000)
                .refreshAfterWrite(carbonClientProperties.getRefreshTimeDuration())
                .buildAsync(
                        new AsyncCacheLoader<String, String>() {
                            @Override
                            public @NonNull CompletableFuture<String> asyncLoad(@NonNull String key, @NonNull Executor executor) {
                                System.out.println("Inside currently live version of RuleSetKey {} " + key);

                                String finalUrl = carbonClientProperties.getBaseUrl()
                                        + "/rules_engine/api/rule_set/" + key + "/rule_set_version/currently_live";

                                return carbonWebClient.get()
                                        .uri(finalUrl)
                                        .header("Authorization", carbonClientProperties.getToken())
                                        .exchange()
                                        .transform(CircuitBreakerOperator.of(circuitBreaker))
                                        .flatMap(clientResponse -> {
                                            if (clientResponse.statusCode() == HttpStatus.NOT_FOUND) {
                                                return clientResponse.toEntity(String.class).flatMap(
                                                        responseEntity -> Mono.just("")
                                                );
                                            }
                                            if (clientResponse.statusCode() != HttpStatus.OK) {
                                                return Mono.error(new RuntimeException("Failed fetching live version for rule set id "
                                                        + key + " got response code: " + clientResponse.statusCode()));
                                            }
                                            return clientResponse.toEntity(String.class).flatMap(
                                                    responseEntity -> {
                                                        String response = responseEntity.getBody();
                                                        if (response != null)
                                                            return Mono.just(response);
                                                        else return Mono.just("");
                                                    }
                                            );
                                        })
                                        .doOnSuccess(responseEntity -> log.debug("Response Struct: {} ", responseEntity))
                                        .toFuture();
                            }
                        }
                );
    }

    @PreDestroy
    public void destroy() {
        scheduledExecutorService.shutdown();
    }

    private void fetchJsScriptAndCache() {
        jsScriptCache = Caffeine
                .newBuilder()
                .maximumSize(10000)
                .refreshAfterWrite(carbonClientProperties.getRefreshTimeDuration())
                .buildAsync(
                        new AsyncCacheLoader<String, Invocable>() {
                            @Override
                            public @NonNull CompletableFuture<Invocable> asyncLoad(@NonNull String key, @NonNull Executor executor) {
                                System.out.println("Script Key {} " + key);
                                String scriptUrl = carbonClientProperties.getBaseUrl() + "/static/evaluate.js";
                                log.debug("Script Url: {}", scriptUrl);
                                return carbonWebClient.get()
                                        .uri(scriptUrl)
                                        .exchange()
                                        .transform(CircuitBreakerOperator.of(circuitBreaker))
                                        .flatMap(clientResponse -> {
                                            if (clientResponse.statusCode() == HttpStatus.NOT_FOUND) {
                                                return clientResponse.toEntity(String.class).flatMap(
                                                        responseEntity -> Mono.just(null)
                                                );
                                            }
                                            if (clientResponse.statusCode() != HttpStatus.OK) {
                                                return Mono.error(new RuntimeException("Failed fetching from source "
                                                        + key + " got response code: " + clientResponse.statusCode()));
                                            }
                                            return clientResponse.toEntity(String.class).flatMap(
                                                    responseEntity -> {
                                                        String script = responseEntity.getBody();
                                                        ScriptEngineManager manager = new ScriptEngineManager();
                                                        ScriptEngine engine = manager.getEngineByName("JavaScript");
                                                        try {
                                                            engine.eval(script);
                                                        } catch (Exception e) {
                                                            e.printStackTrace();
                                                            System.out.println("Unable to parse the js code");
                                                        }
                                                        Invocable invocableService = (Invocable) engine;
                                                        return Mono.just(invocableService);
                                                    }
                                            );
                                        })
                                        .doOnSuccess(response -> log.debug("Response Struct: {} " + response))
                                        .toFuture();
                            }

                            @Override
                            public @NonNull CompletableFuture<Invocable> asyncReload(@NonNull String key, @NonNull Invocable oldValue, @NonNull Executor executor) {
                                System.out.println("Inside Reload " + key);
                                return Mono.fromFuture(asyncLoad(key, executor)).map(
                                        script -> {
                                            if (script == null || script.equals("")) {
                                                return oldValue;
                                            }
                                            return script;
                                        }
                                ).onErrorReturn(oldValue).toFuture();
                            }
                        }
                );
    }

    private void fetchRuleSetVersionsFromRuleEngine() {
        ruleSetVersionsCache = Caffeine
                .newBuilder()
                .maximumSize(10000)
                .refreshAfterWrite(carbonClientProperties.getRefreshTimeDuration())
                .buildAsync(
                        new AsyncCacheLoader<Key, String>() {
                            @Override
                            public @NonNull CompletableFuture<String> asyncLoad(@NonNull Key key, @NonNull Executor executor) {

                                System.out.println("RuleSetKey {} " + key.toString());
                                String finalUrl = carbonClientProperties.getBaseUrl() + "/rules_engine/api/rule_set_versions/" + key.getRuleSetVersionId();

                                return carbonWebClient.get()
                                        .uri(finalUrl)
                                        .header("Authorization", carbonClientProperties.getToken())
                                        .exchange()
                                        .transform(CircuitBreakerOperator.of(circuitBreaker))
                                        .flatMap(clientResponse -> {
                                            if (clientResponse.statusCode() == HttpStatus.NOT_FOUND) {
                                                return clientResponse.toEntity(String.class).flatMap(
                                                        responseEntity -> Mono.just("")
                                                );
                                            }
                                            if (clientResponse.statusCode() != HttpStatus.OK) {
                                                return Mono.error(new RuntimeException("Failed fetching from source rule set version id "
                                                        + key.getRuleSetVersionId() + " got response code: " + clientResponse.statusCode()));
                                            }
                                            return clientResponse.toEntity(String.class).flatMap(
                                                    //responseEntity -> Mono.just(responseEntity.getBody())
                                                    responseEntity -> {

                                                        String response = responseEntity.getBody();
                                                        JSONObject responseObject = new JSONObject(response);
                                                        if (responseObject.isNull("rule_set_version")) {
                                                            return Mono.just(responseObject.toString());
                                                        }
                                                        JSONObject ruleSetVersion = responseObject.getJSONObject("rule_set_version");
                                                        return Mono.just(ruleSetVersion.toString());
                                                    }
                                            );
                                        })
                                        .doOnSuccess(responseEntity -> log.debug("Response Struct: {} " + responseEntity))
                                        .toFuture();
                            }

                            @Override
                            public @NonNull CompletableFuture<String> asyncReload(@NonNull Key key, @NonNull String oldValue, @NonNull Executor executor) {
                                System.out.println("Reload Key " + key.toString());
                                return Mono.fromFuture(asyncLoad(key, executor)).map(
                                        rule -> {
                                            if (rule == null || rule.equals("")) {
                                                return oldValue;
                                            }
                                            return rule;
                                        }
                                ).onErrorReturn(oldValue).toFuture();
                            }
                        }
                );
    }


    public Mono<Object> getRuleSetVersions(String ruleSetId, String ruleSetVersionId) {
        Key key = new Key(ruleSetId, ruleSetVersionId);
        System.out.println("Get RuleSetVersion: " + key.toString());
        return Mono.fromFuture(ruleSetVersionsCache.get(key).toCompletableFuture())
                .map(ruleSetVersion -> {
                    if (ruleSetVersion == null || ruleSetVersion.equals("")) {
                        return "{}";
                    }
                    return Mono.just(ruleSetVersion);
                }).onErrorReturn("{}");
    }

    public Object evaluate(String ruleSetId, String ruleSetVersionId, Map<String, Object> facts, boolean showTestResults) {

        if (ruleSetId == null || ruleSetId.equals("")) {
            return new Response("1", "ruleSetId is required");
        }

        if (facts == null) {
            return new Response("1", "facts is required");
        }

        JSONObject jsonObject = new JSONObject(facts);

        if (ruleSetVersionId == null || ruleSetVersionId.equals("")) {
            return evaluateLatest(ruleSetId, jsonObject.toString(), showTestResults);
        } else {
            return evaluateVersion(ruleSetId, ruleSetVersionId, jsonObject.toString(), showTestResults);
        }
    }

    public Object evaluateVersion(String ruleSetId, String ruleSetVersionId, String facts, boolean showTestResults) {

        Key key = new Key(ruleSetId, ruleSetVersionId);

        System.out.println("Inside evaluateVersion " + key);
        return jsScriptCache.get(SCRIPT_KEY).thenCombine(ruleSetVersionsCache.get(key), (script,ruleSetVersion)->{
            try {
                Object result = script.invokeFunction("evaluate", ruleSetId, ruleSetVersion, facts, ruleSetVersionId, showTestResults);
                return new ObjectMapper().readValue(result.toString(), HashMap.class);
            } catch (Exception e) {
                e.printStackTrace();
                return new Response("1",e.getMessage());
            }
        });
    }

    public Object evaluateLatest(String ruleSetId, String facts, boolean showTestResults) {
        System.out.println("Inside evaluateLatest " + ruleSetId);
        return jsScriptCache.get(SCRIPT_KEY).thenCombine(currentlyLiveSetVersionCache.get(ruleSetId), (script, ruleSetVersion)->{
            try {
                if (ruleSetVersion.equals("null")) {
                    return new Response("1", "No valid live version is present for the id provided");
                }
                JSONObject ruleSetVersionObject = new JSONObject(ruleSetVersion);
                String ruleSetVersionId = ruleSetVersionObject.get("id").toString();
                Object result = script.invokeFunction("evaluate", ruleSetId, ruleSetVersion, facts, ruleSetVersionId, showTestResults);
                return new ObjectMapper().readValue(result.toString(), HashMap.class);
            } catch (Exception e) {
                e.printStackTrace();
                return new Response("1",e.getMessage());
            }
        });
    }
}
