package com.pharmeasy.carbon.client.config;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

@ConfigurationProperties(prefix = "com.pharmeasy.carbon.client")
@Getter
@Setter
public class CarbonClientProperties {

    private String baseUrl = "http://localhost:4000";

    private String token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjEwMDAwMDAwMDAwMCwidXNlcl9pZCI6NDUsImF1ZCI6Ikpva2VuIiwiZXhwIjoxNjE1ODk1MzU0LCJpYXQiOjE2MTU4ODgxNTQsImlzcyI6Ikpva2VuIiwianRpIjoiMnBtY2pkcDdhb2Joc2VpdW5jMDAwNnI0IiwibmJmIjoxNjE1ODg4MTU0fQ.FF4PP1LKX1WoseCA3Vsp0FE58EitvA-fx8kyGo6rVds";

    private List<String> exchangeFilterFunctionBeans = new ArrayList<>();

    private Duration refreshTimeDuration = Duration.ofMinutes(5);

    private Duration connectTimeout = Duration.ofSeconds(2);

    private Duration readTimeout = Duration.ofSeconds(1);

    private Duration pollerReadTimeout =  Duration.ofSeconds(30);

}

