package com.pharmeasy.carbon.client.config;

import io.netty.channel.ChannelOption;
import io.netty.handler.timeout.ReadTimeoutHandler;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.reactive.ClientHttpConnector;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.netty.http.client.HttpClient;

import java.time.temporal.ChronoUnit;
import java.util.concurrent.TimeUnit;

@Configuration
@EnableConfigurationProperties(CarbonClientProperties.class)
public class CarbonClientConfig {

    private ClientHttpConnector getClientHttpConnector(CarbonClientProperties abClientProperties, ApplicationContext context, WebClient.Builder builder, HttpClient httpClient) {
        // create a client http connector using above http client
        ClientHttpConnector connector = new ReactorClientHttpConnector(httpClient);
        if (abClientProperties.getExchangeFilterFunctionBeans() != null) {
            for (String bean : abClientProperties.getExchangeFilterFunctionBeans()) {
                builder.filter(((ExchangeFilterFunction) context.getBean(bean)));
            }
        }
        return connector;
    }

    @Bean(name = "carbonWebClient")
    public WebClient carbonWebClient(CarbonClientProperties carbonClientProperties, ApplicationContext context) {
        WebClient.Builder carbonBuilder = WebClient.builder().baseUrl(carbonClientProperties.getBaseUrl());
        HttpClient carbonHttpClient = HttpClient.create()
                .tcpConfiguration(tcpClient -> {
                    tcpClient = tcpClient.option(ChannelOption.CONNECT_TIMEOUT_MILLIS,
                            (int) carbonClientProperties.getConnectTimeout().get(ChronoUnit.SECONDS) * 1000);
                    tcpClient = tcpClient.doOnConnected(conn -> conn
                            .addHandlerLast(new ReadTimeoutHandler((int) carbonClientProperties
                                    .getReadTimeout().get(ChronoUnit.SECONDS), TimeUnit.SECONDS)));
                    return tcpClient;
                });
        ClientHttpConnector connector = getClientHttpConnector(carbonClientProperties, context, carbonBuilder, carbonHttpClient);
        return WebClient.builder()
                .baseUrl(carbonClientProperties.getBaseUrl())
                .clientConnector(connector).build();
    }

    @Bean(name = "carbonPollerWebClient")
    public WebClient carbonPollerWebClient(CarbonClientProperties carbonClientProperties, ApplicationContext context) {
        WebClient.Builder carbonBuilder = WebClient.builder().baseUrl(carbonClientProperties.getBaseUrl());
        HttpClient carbonHttpClient = HttpClient.create()
                .tcpConfiguration(tcpClient -> {
                    tcpClient = tcpClient.option(ChannelOption.CONNECT_TIMEOUT_MILLIS,
                            (int) carbonClientProperties.getConnectTimeout().get(ChronoUnit.SECONDS) * 1000);
                    tcpClient = tcpClient.doOnConnected(conn -> conn
                            .addHandlerLast(new ReadTimeoutHandler((int) carbonClientProperties
                                    .getPollerReadTimeout().get(ChronoUnit.SECONDS), TimeUnit.SECONDS)));
                    return tcpClient;
                });
        ClientHttpConnector connector = getClientHttpConnector(carbonClientProperties, context, carbonBuilder, carbonHttpClient);
        return WebClient.builder()
                .baseUrl(carbonClientProperties.getBaseUrl())
                .clientConnector(connector).build();
    }
}
