package com.pharmeasy.carbon.client.pollers;

import com.github.benmanes.caffeine.cache.AsyncLoadingCache;
import com.pharmeasy.carbon.client.util.ScriptExecutorService;
import org.springframework.stereotype.Component;

import javax.script.Invocable;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
public class ScriptPoller {

    private final static String SCRIPT_KEY = "JS_CODE";

    public void startScriptPoller(ScheduledExecutorService scheduledExecutorService, AsyncLoadingCache<String, Invocable> jsScriptCache) {
        System.out.println("Inside startScriptPoller");
        Runnable r = () -> {
            System.out.println("startScriptPoller started");
            try {
                jsScriptCache.synchronous().refresh(SCRIPT_KEY);
            } catch (Exception e) {
                System.out.println("error on startScriptPoller: " + e.getMessage());
            }
        };
        scheduledExecutorService.scheduleWithFixedDelay(r, 5, 60, TimeUnit.SECONDS);
    }
}
