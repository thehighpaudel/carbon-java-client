package com.pharmeasy.carbon.client.pollers;

import com.github.benmanes.caffeine.cache.AsyncLoadingCache;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
@RequiredArgsConstructor
public class RuleSetVersionPoller {
    public void startRuleSetVersionPoller(ScheduledExecutorService scheduledExecutorService,
                                          AsyncLoadingCache<String, String> currentlyLiveSetVersionCache) {
        System.out.println("Inside CurrentlyLiveruleSetVersionPoller");
        Runnable r = () -> {
            System.out.println("CurrentlyLiveruleSetVersionPoller started");
            try {
                List<String> keySet = new ArrayList<>();

                for (Map.Entry<String, String> map : currentlyLiveSetVersionCache.synchronous().asMap().entrySet()) {
                    keySet.add(map.getKey());
                }

                for (String key: keySet) {
                    System.out.println("Refreshing : " + key);
                    currentlyLiveSetVersionCache.synchronous().refresh(key);
                }
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("error on CurrentlyLiveruleSetVersionPoller: " + e.getMessage());
            }
        };
        scheduledExecutorService.scheduleWithFixedDelay(r, 5, 60, TimeUnit.SECONDS);
    }
}
