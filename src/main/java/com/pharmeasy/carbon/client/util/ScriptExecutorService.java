package com.pharmeasy.carbon.client.util;

import javax.script.*;

public class ScriptExecutorService {
    private static Invocable invocableService;

    private ScriptExecutorService() {
    }

    public static Invocable getInstance(String jsCode) {
        long startTime = System.currentTimeMillis();
        /*
         * jsCode check is to force create new object even if it exist as
         * the jsCode is being refreshed and so we need new compiled object for new jsCode
         */
        if (jsCode != null) {
            System.out.println("Inside new object creation");
            ScriptEngineManager manager = new ScriptEngineManager();
            ScriptEngine engine = manager.getEngineByName("JavaScript");
            try {
                engine.eval(jsCode);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Unable to parse the js code");
            }
            invocableService = (Invocable) engine;
        }
        long endTime = System.currentTimeMillis();
        long timeElapsed = endTime - startTime;

        System.out.println("Object returned in milliseconds: " + timeElapsed + " hashCode: " + invocableService.hashCode());
        return invocableService;
    }
}