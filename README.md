#Carbon Java Client


## Dependency

```xml
<dependency>
  <groupId>com.pharmeasy.carbon.client</groupId>
  <artifactId>carbon-client</artifactId>
  <version>$latestVersion</version>
</dependency>
```

## Configure

Add configuration to application.properties

```properties
    com.pharmeasy.carbon.client.base-url="http://localhost:4000"

    com.pharmeasy.carbon.client.token="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjEwMDAwMDAwMDAwMCwidXNlcl9pZCI6NDUsImF1ZCI6Ikpva2VuIiwiZXhwIjoxNjE1ODk1MzU0LCJpYXQiOjE2MTU4ODgxNTQsImlzcyI6Ikpva2VuIiwianRpIjoiMnBtY2pkcDdhb2Joc2VpdW5jMDAwNnI0IiwibmJmIjoxNjE1ODg4MTU0fQ.FF4PP1LKX1WoseCA3Vsp0FE58EitvA-fx8kyGo6rVds"

   com.pharmeasy.carbon.client.exchange-filter-function-beans=beain1,bean2

    com.pharmeasy.carbon.client.refresh-time-duration=15m

    com.pharmeasy.carbon.client.connect-timeout=2s

    com.pharmeasy.carbon.client.read-timeout=1s

    com.pharmeasy.carbon.client.poller-read-timeout=5m
```
### Development

Release java client
```shell script


 ./gradlew clean install createRelease

 ./gradlew clean publish 
```
